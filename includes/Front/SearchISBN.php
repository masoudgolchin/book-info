<?php
namespace Front;

if (!defined('ABSPATH')) {
    exit;
} // Exit on direct access

/**
* Class SearchISBN
*
*/
class SearchISBN
{
    static function Search()
    {
        if(isset($_GET['isbn'])) {
            $isbn = sanitize_text_field($_GET['isbn']);
            if(empty($isbn)) {
                echo __('type your ISBN in the form!', 'book-info');
            } else {
                if(strlen($isbn) != 13) {
                    echo __( 'ISBN must have 13 charachters!', 'book-info' );
                } else {
                    self::render_search($isbn);
                }
            }
        }
    }

    static function do_search($isbn)
    {
        global $wpdb;
        $books_table = $wpdb->prefix . "books_info";

        $post_id = $wpdb->get_var( $wpdb->prepare( 
                                    "
                                        SELECT post_id 
                                        FROM $books_table 
                                        WHERE isbn = %s
                                    ", 
                                    $isbn
                                ));

        return $post_id;
    }

    static function render_search($isbn)
    {
        $post_id = self::do_search($isbn);
        if($post_id == NULL) {
            echo __('Nothing Found!', 'book-info');
        } else {
?>
        <div class="book">
            <h2><a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></h2>
            <p><?=  __('ISBN: ', 'book-info') . $isbn ?></p><br />
        </div>
<?php
        }
    }
}
