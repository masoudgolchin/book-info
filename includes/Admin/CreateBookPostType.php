<?php
namespace Admin;

if (!defined('ABSPATH')) {
    exit;
} // Exit on direct access

/**
* Class CreateBookPostType
*
*/
class CreateBookPostType
{
    /**
    * Create Book Post Type
    *
    * @return void
    */
    static function create()
    {
        register_post_type( 'book',
            [
              'labels' => [
                'name' => __( 'Books', 'book-info'  ),
                'singular_name' => __( 'Book', 'book-info'  )
              ],
              'public' => true,
              'has_archive' => true,
            ]
        );
    }
}
