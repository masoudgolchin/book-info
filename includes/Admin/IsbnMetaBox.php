<?php
namespace Admin;

if (!defined('ABSPATH')) {
    exit;
} // Exit on direct access

class IsbnMetaBox
{
    /**
    * Create ISBN Meta Box
    * @see https://developer.wordpress.org/reference/functions/add_meta_box/
    * @return void
    */
    static function create_meta_box()
    {
        add_meta_box( 'isbn-id', __( 'Isbn', 'book-info' ), [__CLASS__,'display_meta_box'], 'book', 'normal', 'high' );
    }

    /**
    * Display Box for ISBN MetaBox
    * 
    * 
    */
    static function display_meta_box($post)
    {
        wp_nonce_field( 'isbn_id_nonce', 'isbn_id_nonce' );
        $value = get_post_meta( $post->ID, '_global_notice', true );
        echo '<input style="width:100%;border:1px solid #eee;" id="isbn_id" name="isbn_id" value="' . self::get_isbn($post->ID) . '">';
    }

    /**
    * Check & Save ISBN
    * 
    * @return void
    */

    static function save_isbn($post_id)
    {
        // Check if nonce is set.
        if (!isset($_POST['isbn_id_nonce'])) {
            return;
        }

        // Verify that the nonce is valid.
        if (!wp_verify_nonce($_POST['isbn_id_nonce'], 'isbn_id_nonce')) {
            return;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) {
            return;
        }

        // Check the user's permissions.
        if ( isset($_POST['post_type']) && $_POST['post_type'] == 'book') {
            if (!current_user_can( 'edit_page', $post_id)) {
                return;
            }
        } else {
            if(!current_user_can('edit_post', $post_id)) {
                return;
            }
        }

        // Make sure isbn_id is set.
        if (!isset($_POST['isbn_id'] )) {
            return;
        }

        // Sanitize (isbn_id) text field.
        $isbn_id = sanitize_text_field($_POST['isbn_id']);

        // Make sure isbn_id has 13 charachters.
        if(strlen($isbn_id) != 13) {
            return;
        }

        //Add isbn_id to db
        self::add_isbn_to_db($isbn_id, $post_id);
    }

    /**
    * Check Post has isbn ot not
    * 
    * @return int
    */

    static function has_isbn($post_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "books_info";

        $has_isbn = $wpdb->get_var( "SELECT ID FROM $table_name where post_id = $post_id" );

        return $has_isbn;
    }

    /**
    * Save ISBN to book_info table
    * 
    * @return void
    */
    static function add_isbn_to_db($isbn_id, $post_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "books_info";

        $data       = ['post_id' => $post_id, 'isbn' => $isbn_id];
        $format     = ['%d', '%s'];

        // Check if current post has ISBN or NOT
        $has_isbn = self::has_isbn($post_id);
        if($has_isbn != NULL) {
            $data['ID'] = $has_isbn;
            $format[2]     = '%d';
        }

        // Insert or Replace ISBN if existed.
        $wpdb->replace($table_name, $data, $format);
    }

    /**
    * Get Post ISBN
    * 
    * @return int
    */

    static function get_isbn($post_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "books_info";

        $isbn = $wpdb->get_var( "SELECT isbn FROM $table_name where post_id = $post_id" );

        return $isbn;
    }
}
