<?php
namespace Admin;

if (!defined('ABSPATH')) {
    exit;
} // Exit on direct access

/**
* Class CreateBookTable
*
*/

class CreateBookTable
{
    // Get Book Database Version
    static function getBookDbVersion() {
        return get_option( "book_db_version" );
    }

    /**
    * Creating book table
    * @return void
    */
    public static function create()
    {
        global $wpdb;

        $table_name      = $wpdb->prefix . "books_info";
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE {$table_name} (
            ID int NOT NULL AUTO_INCREMENT,
            post_id int NOT NULL,
            isbn varchar(13) NOT NULL,
            PRIMARY KEY  (ID)
        ) {$charset_collate};";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( $sql );

        add_option('book_db_version', '1.0');
    }
}
