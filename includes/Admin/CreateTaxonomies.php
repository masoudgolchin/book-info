<?php
namespace Admin;

if (!defined('ABSPATH')) {
    exit;
} // Exit on direct access

/**
* Class CreateTaxonomies
*
*/

class CreateTaxonomies
{
    static function create()
    {
        self::authors();
        self::publishers();
    }

    /**
    * Create Authors Taxonomy
    * @see https://codex.wordpress.org/Function_Reference/register_taxonomy
    * @return void
    */
    static function authors()
    {
        $labels = [
            'name'                       => _x( 'Authors', 'taxonomy general name', 'wp-library' ),
            'singular_name'              => _x( 'Author', 'taxonomy singular name', 'wp-library' ),
            'search_items'               => __( 'Search Authors', 'book-info' ),
            'popular_items'              => __( 'Popular Authors', 'book-info' ),
            'all_items'                  => __( 'All Authors', 'book-info' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Author', 'book-info' ),
            'update_item'                => __( 'Update Author', 'book-info' ),
            'add_new_item'               => __( 'Add New Author', 'book-info' ),
            'new_item_name'              => __( 'New Author Name', 'book-info' ),
            'separate_items_with_commas' => __( 'Separate authors with commas', 'book-info' ),
            'add_or_remove_items'        => __( 'Add or remove authors', 'book-info' ),
            'choose_from_most_used'      => __( 'Choose from the most used authors', 'book-info' ),
            'not_found'                  => __( 'No authors found.', 'book-info' ),
            'menu_name'                  => __( 'Authors', 'book-info' ),
        ];

        $args = [
            'hierarchical'          => true,
            'labels'                => $labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => ['slug' => 'authors'],
        ];


        register_taxonomy('Authors', ['book'], $args);
    }

    /**
    * Create Publishers Taxonomy
    * @see https://codex.wordpress.org/Function_Reference/register_taxonomy
    * @return void
    */
    static function publishers()
    {
    
        $labels = [
            'name'                       => _x( 'Publishers', 'taxonomy general name', 'book-info' ),
            'singular_name'              => _x( 'Publisher', 'taxonomy singular name', 'book-info' ),
            'search_items'               => __( 'Search Publishers', 'book-info' ),
            'popular_items'              => __( 'Popular Publishers', 'book-info' ),
            'all_items'                  => __( 'All Publishers', 'book-info' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Publisher', 'book-info' ),
            'update_item'                => __( 'Update Publisher', 'book-info' ),
            'add_new_item'               => __( 'Add New Publisher', 'book-info' ),
            'new_item_name'              => __( 'New Publisher Name', 'book-info' ),
            'separate_items_with_commas' => __( 'Separate publishers with commas', 'book-info' ),
            'add_or_remove_items'        => __( 'Add or remove publishers', 'book-info' ),
            'choose_from_most_used'      => __( 'Choose from the most used publishers', 'book-info' ),
            'not_found'                  => __( 'No publishers found.', 'book-info' ),
            'menu_name'                  => __( 'Publishers', 'book-info' ),
        ];

        $args = [
            'hierarchical'          => true,
            'labels'                => $labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => ['slug' => 'publishers'],
        ];


        register_taxonomy('Publishers', ['book'], $args);
    }

}
