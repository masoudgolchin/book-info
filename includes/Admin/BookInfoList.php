<?php
// namespace Admin;
if (!defined('ABSPATH')) {
    exit;
} // Exit on direct access

if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class BookInfoList extends WP_List_Table
{
    /** Class constructor */
    public function __construct()
    {
        parent::__construct( [
            'singular' => __( 'Book', 'book-info' ), //singular name of the listed records
            'plural'   => __( 'Books', 'book-info' ), //plural name of the listed records
            'ajax'     => false //should this table support ajax?

        ] );
    }

    public function get_columns()
    {
        $columns = [
            'ID' => __( 'ID', 'book-info' ),
            'post'    => __( 'Post Title', 'book-info' ),
            'isbn'      => __( 'ISBN', 'book-info' )
        ];
        return $columns;
    }

    public function prepare_items()
    {
        $columns    = $this->get_columns();
        $hidden     = [];
        $sortable   = [];
        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $this->get_data_from_db();
    }

    public function column_default( $item, $column_name )
    {
        switch( $column_name ) { 
            case 'ID':
            case 'post':
            case 'isbn':
                return $item[$column_name];
            default:
                return print_r($item, true) ; //Show the whole array for troubleshooting purposes
        }
    }

    public function get_data_from_db()
    {
        global $wpdb;
        $books_table = $wpdb->prefix . "books_info";
        $posts_table = $wpdb->prefix . "posts";

        // Notice: Get post_title based on book_info.post_id
        $sql = "SELECT {$books_table}.ID AS ID,
                        {$posts_table}.post_title AS post,
                        {$books_table}.isbn AS isbn
                        FROM {$books_table}
                        INNER JOIN {$posts_table} ON {$books_table}.post_id = {$posts_table}.ID";

        $results = $wpdb->get_results($sql,ARRAY_A);

        return $results;
    }
}
