<?php
/**
 * Plugin Name: Wordpress Library
 * Description: A Sample WordPress Library Plugin
 * Plugin URI:  https://golchin.me
 * Version:     1.0
 * Author:      Masoud Golchin
 * Author URI:  https://golchin.me
 * License:     MIT
 * Text Domain: book-info
 * Domain Path: /languages
 */

use Admin\IsbnMetaBox;
use Admin\CreateBookTable;
use Admin\CreateTaxonomies;
use Admin\CreateBookPostType;

use Front\SearchISBN;

/* As wordpress did not use psr-4 autoloading
 * I had to include this class manualy
 */
require_once 'includes/Admin/BookInfoList.php';

add_action('plugins_loaded', array(Book_Info::get_instance(), 'plugin_setup'));
add_action('init', [Book_Info::get_instance(), 'init_hooks']);
add_action( 'add_meta_boxes', [Book_Info::get_instance(), 'meta_box_hooks'] );
add_action( 'save_post', [Book_Info::get_instance(), 'save_meta_box'] );
add_action( 'admin_menu', [Book_Info::get_instance(), 'add_book_info_menu'] );
add_filter('the_content', [Book_Info::get_instance(), 'add_isbn_to_content']);
add_shortcode('search_isbn', [Book_Info::get_instance(), 'search_isbn_shortcode']);

class Book_Info
{
    /**
     * Plugin instance.
     *
     * @see get_instance()
     * @type object
     */
    protected static $instance = NULL;
    /**
     * URL to this plugin's directory.
     *
     * @type string
     */
    public $plugin_url = '';
    /**
     * Path to this plugin's directory.
     *
     * @type string
     */
    public $plugin_path = '';

    /**
     * Plugin Version Number
     *
     * @type string
     */
    public $plugin_ver = '1.0';

    /**
     * Access this plugin’s working instance
     *
     * @wp-hook plugins_loaded
     * @since   2012.09.13
     * @return  object of this class
     */
    public static function get_instance()
    {
        NULL === self::$instance and self::$instance = new self;
        return self::$instance;
    }

    /**
     * Used for regular plugin work.
     *
     * @wp-hook plugins_loaded
     * @return  void
     */
    public function plugin_setup()
    {
        $this->plugin_url = plugins_url('/', __FILE__);
        $this->plugin_path = plugin_dir_path(__FILE__);
        $this->load_language('book-info');

        spl_autoload_register(array($this, 'autoload'));

        // Create Book Table on DB
        if($this->plugin_ver != CreateBookTable::getBookDbVersion()) {
            CreateBookTable::create();
        }
    }

    /**
     * Constructor. Intentionally left empty and public.
     *
     * @see plugin_setup()
     */
    public function __construct()
    {
    }

    /**
     * Initial plugin setup
     *
     * @wp-hook init
     * @return  void
     */
    public function init_hooks()
    {
        // Create Book Post Type
        CreateBookPostType::create();

        // Create Authors & Publishers Taxonomies
        CreateTaxonomies::create();
    }

    /**
    * Add Metaboxes
    *
    * @wp-hook add_meta_boxes
    * @return  void
    */
    public function meta_box_hooks() {
        IsbnMetaBox::create_meta_box();
    }

    /**
    * Save Metaboxes
    *
    * @wp-hook add_meta_boxes
    * @return  void
    */
    public function save_meta_box($post_id)
    {
        IsbnMetaBox::save_isbn($post_id);
    }

    /**
    * Add Menu to Dashboard for Book Info List
    *
    * @return  void
    */
    public function add_book_info_menu()
    {
        $page_title = 'Book Info List';
        $menu_title = 'Book Info';
        $capability = 'edit_posts';
        $menu_slug  = 'book_info_list';
        add_menu_page( $page_title, $menu_title, $capability, $menu_slug, [$this, 'render_book_info_page']);
    }

    /**
    * Render Book info page
    *
    * @return  void
    */
    public function render_book_info_page()
    {
        $ISBNList = new BookInfoList();
        echo '<div class="wrap"><h2>' . __( 'Book Info List', 'book-info' ) . '</h2>'; 
        $ISBNList->prepare_items(); 
        $ISBNList->display(); 
        echo '</div>';
    }

    /**
    * Appending ISBN to $content
    *
    * @return  string
    */
    public function add_isbn_to_content($content)
    {
        global $post;
        if ($post->post_type == 'book') {
            $isbn = "<br /><p>ISBN: " . IsbnMetaBox::get_isbn($post->ID) . "</p>";
            $content .= $isbn;
        }
        return $content;
    }

    /**
    * Search ISBN Shortcode
    *
    */
    public function search_isbn_shortcode()
    {
        SearchISBN::Search();
    }

    /**
     * Loads translation file.
     *
     * Accessible to other classes to load different language files (admin and
     * front-end for example).
     *
     * @wp-hook init
     * @param   string $domain
     * @return  void
     */
    public function load_language($domain)
    {
        load_plugin_textdomain($domain, FALSE, $this->plugin_path . '/languages');
    }

    /**
     * @param $class
     *
     */
    public function autoload($class)
    {
        $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);

        if (!class_exists($class)) {
            $class_full_path = $this->plugin_path . 'includes/' . $class . '.php';

            if (file_exists($class_full_path)) {
                require $class_full_path;
            }
        }
    }
}
